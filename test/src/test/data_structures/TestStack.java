package test.data_structures;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class TestStack extends TestCase {
	private Stack<String> lista;

	public void setupEscenario1( )
	{
		lista = new Stack<String>();
	}

	public void setupEscenario2( )
	{
		lista = new Stack<String>();
		lista.push("Yo");
	}

	public void setupEscenario3( )
	{
		lista = new Stack<String>();
		lista.push("Yo");
		lista.push("Tu");
		lista.push("El");
		lista.push("Ella");
		lista.push("Ustedes");
		lista.push("Nosotros");
		lista.push("Eso");
		lista.push("Esa");
		lista.push("Vos");
		lista.push("Vosotros");
	}
	public void testPush1(){
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", lista.pop());
	}

	public void testPush2(){
		setupEscenario2();
		lista.push("Tu");
		lista.push("El");
		lista.push("Ella");
		ArrayList<String> g=new ArrayList<>();
		for (int i = lista.size(); i > 0 ; i--) {
			g.add(lista.pop());
		}
		assertEquals("El objeto no coincide con el esperado", "Tu" , g.get(2));
		assertEquals("El objeto no coincide con el esperado", "El" , g.get(1));
		assertEquals("El objeto no coincide con el esperado", "Ella" , g.get(0));
		assertEquals("El objeto no coincide con el esperado", "Yo" , g.get(3));
		
	}

	public void testIsEmpty1(){
		setupEscenario1();
		assertEquals("Queue debe estar vacia", true, lista.isEmpty());
	}
	public void testIsEmpty2(){
		setupEscenario2();
		assertEquals("Queue no deberia estar vacia", false, lista.isEmpty());
	}
	public void testIsEmpty3(){
		setupEscenario3();
		assertEquals("Queue no deberia estar vacia", false, lista.isEmpty());
	}

	public void testSize1(){
		setupEscenario1();
		lista.push("yo");
		lista.pop();
		assertEquals("Numero de elemntos erroneo", 0 , lista.size());
	}

	public void testSize2(){
		setupEscenario2();
		assertEquals("Numero de elemntos erroneo", 1 , lista.size());
		
	}

	public void testSize3(){
		setupEscenario3();
		assertEquals("Numero de elemntos erroneo", 10 , lista.size());
	}

}


package test.data_structures;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class TestQueue extends TestCase{

	private Queue<String> lista;

	public void setupEscenario1( )
	{
		lista = new Queue<String>();
	}

	public void setupEscenario2( )
	{
		lista = new Queue<String>();
		lista.enqueue("Yo");
	}

	public void setupEscenario3( )
	{
		lista = new Queue<String>();
		lista.enqueue("Yo");
		lista.enqueue("Tu");
		lista.enqueue("El");
		lista.enqueue("Ella");
		lista.enqueue("Ustedes");
		lista.enqueue("Nosotros");
		lista.enqueue("Eso");
		lista.enqueue("Esa");
		lista.enqueue("Vos");
		lista.enqueue("Vosotros");
	}
	public void testEnqueue1(){
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", lista.dequeue());
	}

	public void testEnqueue2(){
		setupEscenario2();
		lista.enqueue("Tu");
		lista.enqueue("El");
		lista.enqueue("Ella");
		ArrayList<String> g=new ArrayList<>();
		int x=lista.size();
		for (int i = 0; i < x; i++) {
			g.add(lista.dequeue());
		}
		assertEquals("El objeto no coincide con el esperado", "Yo" , g.get(0));
		assertEquals("El objeto no coincide con el esperado", "Tu" , g.get(1));
		assertEquals("El objeto no coincide con el esperado", "El" , g.get(2));
		assertEquals("El objeto no coincide con el esperado", "Ella" , g.get(3));
	}

	public void testIsEmpty1(){
		setupEscenario1();
		assertEquals("Queue debe estar vacia", true, lista.isEmpty());
	}
	public void testIsEmpty2(){
		setupEscenario2();
		assertEquals("Queue no deberia estar vacia", false, lista.isEmpty());
	}
	public void testIsEmpty3(){
		setupEscenario3();
		assertEquals("Queue no deberia estar vacia", false, lista.isEmpty());
	}

	public void testSize1(){
		setupEscenario1();
		lista.enqueue("yo");
		lista.dequeue();
		assertEquals("Numero de elemntos erroneo", 0 , lista.size());
	}

	public void testSize2(){
		setupEscenario2();
		assertEquals("Numero de elemntos erroneo", 1 , lista.size());
		
	}

	public void testSize3(){
		setupEscenario3();
		assertEquals("Numero de elemntos erroneo", 10 , lista.size());
	}

}

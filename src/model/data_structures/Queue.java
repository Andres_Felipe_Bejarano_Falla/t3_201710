package model.data_structures;

public class Queue<T> {
	private ListaEncadenada<T> base;
	public Queue() {
		base=new ListaEncadenada<T>();
	}
	public void enqueue(T item){
		base.agregarElementoFinal(item);
	}
	public T dequeue(){
		T joder=base.darElemento(0);
		base.eliminarInicial();
		base.size--;
		return joder;
	}
	public int size()
	{
		return base.size;
	}
	public boolean isEmpty(){
		if(base.size==0)
			return true;
		else return false;
	}
}

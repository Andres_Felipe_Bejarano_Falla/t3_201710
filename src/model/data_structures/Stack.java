package model.data_structures;

public class Stack<T> {
	private ListaEncadenada<T> base;
	public Stack() {
		base=new ListaEncadenada<>();
	}
	public void push(T item){
		base.agregarElementoFinal(item);
	}
	public T pop()
	{
		T temo=base.darElemento(base.size-1);
		base.eliminarFinal();
		base.current=base.first;
		return temo;
	}
	public boolean isEmpty(){
		if(base.size==0)
			return true;
		else
			return false;
	}
	public int size(){
		return base.size;
	}
}

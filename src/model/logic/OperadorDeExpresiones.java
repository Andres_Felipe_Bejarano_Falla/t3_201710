package model.logic;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class OperadorDeExpresiones {
	Queue<Character> temp;
	Stack<Character> temp1;
	private void resource(String p){
		this.temp=new Queue<>();
		char[] temp=p.toCharArray();
		for (char c : temp) {
			this.temp.enqueue(c);
		}
	}
	private void resource1(String p){
		this.temp1=new Stack<>();
		char[] temp=p.toCharArray();
		for (char c : temp) {
			this.temp1.push(c);
		}
	}

	public boolean expresionBienformada(String p){
		resource1(p);
		int x=0;
		int y=0;
		int z=0;
		while(!temp1.isEmpty()){
			char g=temp1.pop();
			if(g=='[')x+=1;
			if(g==']')x-=1;
			if(g=='(')y+=1;
			if(g==')')y-=1;
			if(g=='+'||g=='*'||g=='/'||g=='-')z+=1;
			else z=0;
			if(z==2||x>0||y>0)return false;
		}
		if(x==0&&y==0){
			return true;
		}
		return false;
	}

	public Stack<Character> ordenarPila(String p){
		resource(p);
		Stack<Character> holi=new Stack<>();
		for (int j = temp.size(); j>0; j--) {
			char tempo=this.temp.dequeue();
			if(tempo=='1'||tempo=='2'||tempo=='3'||tempo=='4'||tempo=='5'||tempo=='6'||tempo=='7'||tempo=='8'||tempo=='9'||tempo=='0'){
				holi.push(tempo);
			}
			else
				temp.enqueue(tempo);
		}
		for (int j = temp.size(); j>0; j--) {
			char tempo=this.temp.dequeue();
			if(tempo=='-'||tempo=='+'||tempo=='/'||tempo=='*'){
				holi.push(tempo);
			}
			else
				temp.enqueue(tempo);
		}
		for (int j = temp.size(); j>0; j--) {
			char tempo=this.temp.dequeue();
			if(tempo=='('||tempo==')'||tempo=='['||tempo==']'){
				holi.push(tempo);
			}
			else
				temp.enqueue(tempo);
		}
		return holi;
	}

	public String concatenar(Stack<Character> i){
		String hola="";
		for (int j = i.size(); j >0; j--) {
			hola+=i.pop()+" ";
		}
		return hola;
	}
}

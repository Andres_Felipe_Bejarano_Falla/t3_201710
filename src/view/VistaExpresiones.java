package view;

import java.util.Scanner;

import controller.Controller;

public class VistaExpresiones {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while (!fin) {
			printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 1: Controller.cambiarExpresion(readData()); System.out.println("Expresion cambiada exitosamente");
			break;
			case 2:	String p1;
			if(Controller.expresionOrdenada())
				p1="correcta";
			else
				p1="incorrecta";
			System.out.println("La expresion es "+p1);
			break;
			case 3:
				System.out.println(Controller.ordenarPila());
				break;
			case 4:
				System.out.println("Adi�s.");
				fin = true;
				break;
			}
		}
	}
	public static String readData(){
		System.out.println("Please type the expresion, then press enter: ");
		String line = new Scanner(System.in).nextLine();
		return line;
	}

	public static void printMenu(){
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("Expresion actual: "+Controller.expresionActual());
		System.out.println("1.A�adir una expresion");
		System.out.println("2.Verificar que la expresion est� correcta");
		System.out.println("3.Retornar la cadena ordenada");
		System.out.println("4.Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}

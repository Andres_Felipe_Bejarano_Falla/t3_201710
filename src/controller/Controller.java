package controller;

import model.logic.OperadorDeExpresiones;

public class Controller {
	private static OperadorDeExpresiones modelo=new OperadorDeExpresiones();
	private static String hola;

	public static void cambiarExpresion(String p){
		hola=p;
	}

	public static boolean expresionOrdenada(){
		if(hola==null)
			return false;
		return modelo.expresionBienformada(hola);
	}

	public static String ordenarPila()
	{
		if(hola==null)
			return "No hay expresion actual";
		return modelo.concatenar(modelo.ordenarPila(hola));
	}
	public static String expresionActual(){
		if(hola==null)
			return "No hay expresion actual";
		return hola;
	}
}
